<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Costumer;
use App\Models\Post;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;

class SystemController extends Controller
{
    public function create(){
        return view('create');
    }

    public function store(Request $request){
        // dd($request->foto);
        $user_id = Auth::user()->id;
        // dd($user_id);

        $store = new Post;
        $store->user_id = $user_id;
        $store->title = $request->creationName;
        $store->body = $request->deskripsi;
        $store->foto = $request->foto;
        $store->save();

        $request->file('foto')->move('img');

        return redirect('/create');
    }

    public function profile(){
        $id_user = Auth::user()->id;

        $profile = DB::table('profiles')->where('user_id', $id_user)->get()->all();
        // dd($profile);
        if($profile == []) {
            return view('profileForm');
        } else {
            return view('profileData', [
                'data' => $profile
            ]);
        }
    }

    public function pStore(Request $request){
        $id_user = Auth::user()->id;

        // dd($request);
        $profile = new Profile;

        $profile->user_id = $id_user;
        $profile->nama_usaha = $request->usaha;
        $profile->nama_pemilik_usaha = $request->nama;
        $profile->nama_izin_usaha = $request->izin_usaha;
        $profile->no_surat_izin_usaha = $request->no_surat;
        $profile->npwp = $request->npwp;
        $profile->tgl_mulai_usaha = $request->tgl_mulai;
        $profile->alamat = $request->alamat;
        $profile->rt = $request->rt;
        $profile->rw = $request->rw;
        $profile->save();

        return redirect('/profile');
    }

    public function pEdit(Request $request, $id){
        // dd($request);

        $profile = Profile::find($id);
        
        $profile->nama_usaha = $request->usaha;
        $profile->nama_pemilik_usaha = $request->nama;
        $profile->nama_izin_usaha = $request->izin_usaha;
        $profile->no_surat_izin_usaha = $request->no_surat;
        $profile->npwp = $request->npwp;
        $profile->tgl_mulai_usaha = $request->tgl_mulai;
        $profile->alamat = $request->alamat;
        $profile->rt = $request->rt;
        $profile->rw = $request->rw;

        $profile->update();

        return redirect('/profile');
    }


    public function show($id){

        $post = Post::find($id);
        return view('show',[
            'post'=> $post
        ]);
    }


    public function myProduct()
    {
        $user_id = Auth::user()->id;
        $produk = Post::where('user_id', $user_id)->get()->all();
        
        // dd($produk);
        return view('myProduct', [
            'produk' => $produk
        ]);
    }

    public function edit($id_produk)
    {
        $data = Post::find($id_produk);
        return view('editProduk', [
            'data' => $data,
            'id_produk' => $id_produk
        ]);
    }

    public function update(Request $request,$id_produk){
        $data = Post::find($id_produk);
        $user_id = Auth::user()->id;

        $data->user_id = $user_id;
        $data->title = $request->creationName;
        $data->body = $request->deskripsi;
        $data->foto = $request->foto;
        $data->update();


        $request->file('foto')->move('img');

        return redirect('/myProduct');
    }


    public function detail($id_produk)
    {
        $produk = Post::find($id_produk);
        return view('detailProduk',[
            'detail' => $produk
        ]);
    }

    public function order(Request $request, $produk_id)
    {
        $produk = Post::find($produk_id);
        $pemilikProduk = $produk->user->id;
        // dd($request);
        $user_id = Auth::user()->id;
        $store = new Costumer;

        $store->user_id = $user_id;
        $store->pemilik_id = $pemilikProduk;
        $store->produk_id = $produk_id;
        $store->jumlah = $request->jml;
        $store->save();

        return redirect('/home');
    }

    public function showCostumers(){
        $pemilik = Auth::user()->id;
        $costumer = Costumer::where('pemilik_id', $pemilik)->get()->all();
        // dd($costumer);
        return view('costumers',[
            'costumers' => $costumer
        ]);
    }


}
