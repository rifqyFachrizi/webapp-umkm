<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','nama_usaha','nama_pemilik_usaha','nama_izin_usaha','no_surat_izin_usaha','npwp','tgl_mulai_usaha','alamat','rt','rw'];
}
