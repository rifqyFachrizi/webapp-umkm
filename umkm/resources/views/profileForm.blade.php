@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="/profile/store" method="POST">
            @csrf
            <div class="mb-3">
                <label for="usaha" class="form-label">Nama Usaha:</label>
                <input type="text" class="form-control" id="usaha" name="usaha" placeholder="nama usaha">
            </div>
            <div class="mb-3">
                <label for="nama" class="form-label">Nama:</label>
                <input type="text" class="form-control" id="nama" name="nama" placeholder="nama">
            </div>
            <div class="mb-3">
                <label for="izin_usaha" class="form-label">Izin Usaha:</label>
                <input type="text" class="form-control" id="izin_usaha" name="izin_usaha" placeholder="izin_usaha">
            </div>
            <div class="row">
                <div class="col">
                    <label for="no_surat" class="form-label">Nomer Surat Izin Usaha:</label>
                    <input type="text" class="form-control" id="no_surat" name="no_surat" placeholder="no_surat">
                </div>
                <div class="col">
                    <label for="npwp" class="form-label">NPWP:</label>
                    <input type="text" class="form-control" id="npwp" name="npwp" placeholder="npwp">
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="tgl_mulai" class="form-label">Tanggal Mulai Usaha:</label>
                    <input type="date" class="form-control" id="tgl_mulai" name="tgl_mulai" placeholder="tgl_mulai">
                </div>
                <div class="col">
                    <label for="alamat" class="form-label">Alamat:</label>
                    <input type="text" class="form-control" id="alamat" name="alamat" placeholder="alamat">
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="rt" class="form-label">RT:</label>
                    <input type="text" class="form-control" id="rt" name="rt" placeholder="rt">
                </div>
                <div class="col">
                    <label for="rw" class="form-label">RW:</label>
                    <input type="text" class="form-control" id="rw" name="rw" placeholder="rw">
                </div>
            </div>
            <div class="mt-3">
                <button class="btn btn-primary form-control" type="submit">Submit!</button>
              </div>
        </form>
    </div>
@endsection