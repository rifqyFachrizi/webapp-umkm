@extends('layouts.app')
@section('content')
<div class="container">
  <h1>My Creation</h1>
    <form action="/myProduct/edit/update/{{ $id_produk }}" method="POST" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="mb-3">
            <label for="creationName" class="form-label">Nama Kreasi :</label>
            <input type="text" class="form-control" id="creationName" name="creationName" placeholder="{{ $data->title }}">
          </div>
          <div class="mb-3">
            <label for="deskripsi" class="form-label">Deskripsi :</label>
            <textarea class="form-control" id="deskripsi" name="deskripsi" rows="3" placeholder="{{ $data->body }}"></textarea>
          </div>
          <div class="mb-3">
            @php
            // dd($data->foto);
            $str = $data->foto;
            $pecah = explode('\\', $str);
            $img = end($pecah);
            // dump($img);
            @endphp
            <label for="foto" class="form-label">Gambar Kreasi</label></br>
            <img src="../../img/{{ $img }}" style="width: 12rem" alt="">
          </div>
          <div class="mb-3">
            <input class="form-control" type="file" id="foto" name="foto">
          </div>
          <div class="mb-3">
            <button class="btn btn-primary form-control" type="submit">Submit!</button>
          </div>
    </form>
</div>
@endsection