@extends('layouts.app')

@section('content')
    <div class="container">
        <table class="table">
            <thead>
              <tr>
                <th scope="col">no</th>
                <th scope="col">nama</th>
                <th scope="col">email</th>
              </tr>
            </thead>
            <tbody>
                @php
                    $i = 1;
                @endphp
                @foreach ($costumers as $item)
                @php
                    // dd($item->user);
                @endphp
                <tr>
                    <th scope="row">{{ $i }}</th>
                    <td>{{ $item->user->name }}</td>
                    <td>{{ $item->user->email }}</td>
                </tr>
                @php
                    $i++;
                @endphp
                @endforeach
            </tbody>
          </table>
    </div>
@endsection