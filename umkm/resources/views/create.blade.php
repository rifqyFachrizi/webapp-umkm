@extends('layouts.app')

@section('content')
<div class="container">
  <h1>My Creation</h1>
    <form action="/create/store" method="POST" enctype="multipart/form-data">
      @csrf
        <div class="mb-3">
            <label for="creationName" class="form-label">Nama Kreasi :</label>
            <input type="text" class="form-control" id="creationName" name="creationName" placeholder="nama dari kreasi/produk">
          </div>
          <div class="mb-3">
            <label for="deskripsi" class="form-label">Deskripsi :</label>
            <textarea class="form-control" id="deskripsi" name="deskripsi" rows="3"></textarea>
          </div>
          <div class="mb-3">
            <label for="foto" class="form-label">Gambar Kreasi</label>
            <input class="form-control" type="file" id="foto" name="foto">
          </div>
          <div class="mb-3">
            <button class="btn btn-primary form-control" type="submit">Submit!</button>
          </div>
    </form>
</div>
@endsection