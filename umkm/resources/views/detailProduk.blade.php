@extends('layouts.app')

@section('content')
@php
        $str = $detail->foto;
        $pecah = explode('\\', $str);
        $img = end($pecah);
        // dd($detail->id);
@endphp
    <div class="container">
        <div class="card">
            <h5 class="card-header">Detail Produk</h5>
            <div class="card-body">
              <h5 class="card-title">{{ $detail->title }}</h5>
              <p class="card-text">{{ $detail->body }}</p>
              <img src="../../img/{{ $img }}" style="width: 12rem" alt=""></br>
              <form action="/order/{{ $detail->id }}">
                <div class="input-group mb-3">
                    <span class="input-group-text mt-3" id="inputGroup-sizing-default">Jumlah</span>
                    <input name="jml" type="text" class="form-control mt-3" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
                </div>
                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                    <button class="btn btn-primary me-md-2" name="submit" type="submit">Order</button>
                  </div>
              </form>
              
            </div>
          </div>
    </div>
@endsection