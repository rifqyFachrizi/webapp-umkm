@extends('layouts.app')

@section('content')
<div class="container card-box">
@foreach ($data as $item)
@php
    // dd($item->id);
    $str = $item->foto;
    $pecah = explode('\\', $str);
    $img = end($pecah);
    //dd($str);
    // dd($item->user->name);
@endphp
<div class="card" style="width: 18rem;">
  <img src="img/{{ $img }}" class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title">{{ $item->title }}</h5>
    <span>Author : {{ $item->user->name }}</span>
    <p class="card-text">{{ $item->body }}</p>
    <a href="/detail/{{ $item->id }}" class="btn btn-primary">Go Detail</a>
    <a href="{{ url('article', $item->id) }}" class="btn btn-secondary">Comment</a>
  </div>
</div>
@endforeach

</div>
@endsection
