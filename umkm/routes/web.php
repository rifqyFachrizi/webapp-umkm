<?php

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\SystemController;
use App\Http\Controllers\CommentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/', function(){
//     redirect('/login');
// });
Route::get('/', function () {
    Auth::logout();
    return view('auth.login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/create', [SystemController::class, 'create']);
Route::post('/create/store', [SystemController::class, 'store']);
Route::get('/profile', [SystemController::class, 'profile']);
Route::post('/profile/store', [SystemController::class, 'pStore']);
Route::put('/profile/edit/{id}', [SystemController::class, 'pEdit']);


Route::get('/article/{post:slug}', [SystemController::class, 'show']);
Route::post('/comment/store', [CommentController::class, 'store']);
Route::post('/reply/store', [CommentController::class, 'replyStore']);

Route::get('/myProduct', [SystemController::class, 'myProduct']);
Route::get('/myProduct/edit/{id_product}', [SystemController::class, 'edit']);
Route::put('/myProduct/edit/update/{id_product}',[SystemController::class, 'update']);

Route::get('/detail/{id}', [SystemController::class, 'detail']);
Route::get('/order/{id_produk}', [SystemController::class, 'order']);

Route::get('/costumers', [SystemController::class, 'showCostumers']);